import { BackgroundChild } from "./components/style/BackgroudStyle";
import { ButtonChild } from "./components/style/ButtonStyle";
import { TextTitle } from "./components/style/TextTitleStyle";

function App() {
  return (
    <BackgroundChild>
      <div>
        <ButtonChild textColor="gray">&lt;</ButtonChild>
        <ButtonChild textColor="gray">&lt;&lt;</ButtonChild>
        <TextTitle>February 2023</TextTitle>
        <ButtonChild textColor="gray">&gt;</ButtonChild>
        <ButtonChild textColor="gray">&gt;&gt;</ButtonChild>
      </div>
      <div>
        <ButtonChild>Sun</ButtonChild>
        <ButtonChild>Mon</ButtonChild>
        <ButtonChild>Tue</ButtonChild>
        <ButtonChild>Wed</ButtonChild>
        <ButtonChild>Thu</ButtonChild>
        <ButtonChild>Fri</ButtonChild>
        <ButtonChild>Sat</ButtonChild>
      </div>
      <div>
        <ButtonChild textColor="#9DA7A5">29</ButtonChild>
        <ButtonChild textColor="#9DA7A5">30</ButtonChild>
        <ButtonChild textColor="#9DA7A5">31</ButtonChild>
        <ButtonChild>1</ButtonChild>
        <ButtonChild>2</ButtonChild>
        <ButtonChild>3</ButtonChild>
        <ButtonChild>4</ButtonChild>
      </div>
      <div>
        <ButtonChild textColor="red">5</ButtonChild>
        <ButtonChild>6</ButtonChild>
        <ButtonChild>7</ButtonChild>
        <ButtonChild>8</ButtonChild>
        <ButtonChild>9</ButtonChild>
        <ButtonChild>10</ButtonChild>
        <ButtonChild>11</ButtonChild>
      </div>
      <div>
        <ButtonChild textColor="red">12</ButtonChild>
        <ButtonChild>13</ButtonChild>
        <ButtonChild>14</ButtonChild>
        <ButtonChild>15</ButtonChild>
        <ButtonChild>16</ButtonChild>
        <ButtonChild>17</ButtonChild>
        <ButtonChild>18</ButtonChild>
      </div>
      <div>
        <ButtonChild textColor="red">19</ButtonChild>
        <ButtonChild>20</ButtonChild>
        <ButtonChild>21</ButtonChild>
        <ButtonChild>22</ButtonChild>
        <ButtonChild>23</ButtonChild>
        <ButtonChild>24</ButtonChild>
        <ButtonChild>25</ButtonChild>
      </div>
      <div>
        <ButtonChild textColor="red">26</ButtonChild>
        <ButtonChild>27</ButtonChild>
        <ButtonChild bgColor="#016FDA" textColor="white">28</ButtonChild>
        <ButtonChild textColor="#9DA7A5">1</ButtonChild>
        <ButtonChild textColor="#9DA7A5">2</ButtonChild>
        <ButtonChild textColor="#9DA7A5">3</ButtonChild>
        <ButtonChild textColor="#9DA7A5">4</ButtonChild>
      </div>
    </BackgroundChild>
  );
}

export default App;
