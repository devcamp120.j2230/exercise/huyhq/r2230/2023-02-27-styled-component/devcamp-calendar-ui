import styled from "styled-components"

const BackgroundParent = styled.div`
        color: #000;
        background: #fff;
        padding: 5px;
        border: 1px solid grey;
        max-width: fit-content;
        margin: 10px auto;
    `

export const BackgroundChild = styled(BackgroundParent)``;