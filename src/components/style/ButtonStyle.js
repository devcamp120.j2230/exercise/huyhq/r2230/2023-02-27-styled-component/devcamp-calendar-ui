import styled from "styled-components"

const ButtonStyleParent = styled.button`
        font-size: 20px;
        width: 70px;
        height: 70px;
        text-align: center;
        padding: 5px;
        color: black;
        background: white;
        text-transform: uppercase;
        border: none;
    `

export const ButtonChild = styled(ButtonStyleParent)`
        color: ${prop => prop.textColor};
        background: ${prop => prop.bgColor};
    `