import styled from "styled-components"

export const TextTitle = styled.div`
        font-size: 20px;
        width: 210px;
        height: 70px;
        text-align: center;
        padding: 5px;
        color: gray;
        background: white;
        display: inline-block;
        line-height: 70px;
    `